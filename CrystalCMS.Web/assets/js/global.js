$(document).ready(function () {
  // resposnive header
  $(".burger").click(function () {
    $(this).toggleClass("active");
    $(".menu").slideToggle("fast");
  });

  // active package
    $(".package-title").on("click", function () {
        if ($(this).attr("id") == "monthly") {
            $(".monthly").show();
            $(".yearly").hide();
        }
        else {
            $(".monthly").hide();
            $(".yearly").show();
        }
    $(this).addClass("active-package").siblings().removeClass("active-package");
  });

  // package active tab
  $(".package-accordion-head").click(function () {
    if ($(".package-accordion-body").is(":visible")) {
      $(".package-accordion-body").slideUp(300);
      $(".plusminus").attr("src", "../../assets/image/Plus Icon.svg");
    }
    if ($(this).next(".package-accordion-body").is(":visible")) {
      $(this).next(".package-accordion-body").slideUp(300);
      $(this)
        .children(".plusminus")
        .attr("src", "../../assets/image/Plus Icon.svg");
    } else {
      $(this).next(".package-accordion-body").slideDown(300);
      $(this)
        .children(".plusminus")
        .attr("src", "../../assets/image/Minus Icon.svg");
    }
  });
});
