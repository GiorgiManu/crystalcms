$(document).ready(function () {
  $(".accordion-head").click(function () {
    $(this).removeClass("faq-active");
    if ($(".accordion-body").is(":visible")) {
      $(".accordion-body").slideUp(300);
      $(".plusminus").attr("src", "../../assets/image/Plus Icon.svg");
      $(".accordion-head").removeClass("faq-active");
    }
    if ($(this).next(".accordion-body").is(":visible")) {
      $(this).next(".accordion-body").slideUp(300);
      $(this)
        .children(".plusminus")
        .attr("src", "../../assets/image/Plus Icon.svg");
      $(this).removeClass("faq-active");
    } else {
      $(this).next(".accordion-body").slideDown(300);
      $(this)
        .children(".plusminus")
        .attr("src", "../../assets/image/Minus Icon.svg");
      $(this).addClass("faq-active");
    }
  });
});
