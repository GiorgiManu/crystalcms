$(document).ready(function () {
  // video popup
  $(".start-video").click(function (e) {
    e.preventDefault();
    $("#video-modal").show();
    $("body").addClass("hidden");
  });
  $("#video-modal").click(function () {
    $("#video-modal").hide();
    $("body").removeClass("hidden");
  });

  // animate css scroll
  function isScrolledIntoView(elem) {
    var docViewTop = $(window).scrollTop();
    var docViewBottom = docViewTop + $(window).height();

    var elemTop = $(elem).offset().top;
    var elemBottom = elemTop + $(elem).height();

    return elemBottom <= docViewBottom && elemTop >= docViewTop;
  }
  $(window).scroll(function () {
    $(".second-scroll-animations .animate__animated").each(function () {
      if (isScrolledIntoView(this) === true) {
        $(this).addClass("animate__fadeInRight");
      }
    });
    $(".third-scroll-animations .animate__animated").each(function () {
      if (isScrolledIntoView(this) === true) {
        $(this).addClass("animate__fadeInLeft");
      }
    });
    $(".fourth-scroll-animations .animate__animated").each(function () {
      if (isScrolledIntoView(this) === true) {
        $(this).addClass("animate__fadeInLeft");
      }
    });
  });

  // active feature
  $(".content-feature").on("click", function () {
    $(this)
      .addClass("active-content-feature")
      .siblings()
      .removeClass("active-content-feature");
  });

  // tabs content
  $("#tabs-nav li:first-child").addClass("active");
  $(".tab-content").hide();
  $(".tab-content:first").show();

  $(".pos-tabs .tabs-nav li").click(function () {
    $(".pos-tabs .tabs-nav li").removeClass("active");
    $(this).addClass("active");
    $(".pos-tab-content").hide();

    var activeTab = $(this).find("a").attr("href");
    $(activeTab).fadeIn();
    return false;
  });
  $(".erp-tabs .tabs-nav li").click(function () {
    $(".erp-tabs .tabs-nav li").removeClass("active");
    $(this).addClass("active");
    $(".erp-tab-content").hide();

    var activeTab = $(this).find("a").attr("href");
    $(activeTab).fadeIn();
    return false;
  });

  // swiper init
  new Swiper(".pos-tabs.swiper-container", {
    direction: "horizontal",
    slidesPerView: 2,
    autoplay: false,
    paginationClickable: true,
  });
  new Swiper(".erp-tabs.swiper-container", {
    direction: "horizontal",
    slidesPerView: 2,
    autoplay: false,
    paginationClickable: true,
  });
});
