$(document).ready(function () {
    // slider init
    var mySwiper = new Swiper(".swiper-container", {
        direction: "horizontal",
        slidesPerView: 1,
        autoplay: false,
        simulateTouch: false,
        touchRatio: 0,
        initialSlide: 0,
        pagination: {
            el: ".swiper-pagination",
            type: "bullets",
            clickable: true,
        },
        navigation: {
            //nextEl: ".swiper-button-next",
            //prevEl: ".swiper-button-prev",
            clickable: true
        }
    });

    $(".swiper-button-prev").click(() => mySwiper.slideTo(0));

    $(document).on('click', '.back-registration', function () {
        //$('input').val('');
        //mySwiper.slideTo(0)
        location.reload(true);
    })

    jQuery.validator.setDefaults({
        debug: true,
        success: "valid",
    });

    // first form validation
    $("#first-registration")
        .submit(function (e) {
            e.preventDefault();
        })

        .validate({
            errorPlacement: function (error, element) {
                return true;
            },
            ignore: [],
            rules: {
                firstName: {
                    required: true,
                },
                lastName: {
                    required: true,
                },
                email: {
                    required: true,
                    email: true,
                },
                number: {
                    required: true,
                    maxlength: 11,
                    minlength: 11,
                },
                mobile: {
                    required: true,
                    maxlength: 9,
                    minlength: 9,
                },
            },
            messages: {
                required: "",
                firstName: jQuery.validator.format(""),
                email: jQuery.validator.format(""),
                number: jQuery.validator.format(""),
                mobile: jQuery.validator.format(""),
            },
            submitHandler: function (form) {
                mySwiper.slideNext();
                // mySwiper.slidePrev();
                $(".swiper-pagination-bullet-active")
                    .next(".swiper-pagination-bullet")
                    .addClass("pointer");
                $(".swiper-pagination-bullet-active")
                    .prev(".swiper-pagination-bullet")
                    .addClass("pointer");
                $(".swiper-pagination-bullet:last-child").addClass("pointer-none");
                return false;
            },
        });
    //second form validation
    $("#second-registration")
        .submit(function (e) {
            e.preventDefault();
        })
        .validate({
            errorPlacement: function (error, element) {
                return true;
            },
            ignore: [],
            rules: {
                companyName: {
                    required: true,
                },
                identificationNumber: {
                    required: true,
                    maxlength: 11,
                    minlength: 9,
                },
                adress: {
                    required: true,
                },
                telNumber: {
                    maxlength: 6,
                    minlength: 6,
                },
                agree: {
                    required: true,
                },
                sources: {
                    required: true
                }
            },
            messages: {
                companyName: jQuery.validator.format(""),
                identificationNumber: jQuery.validator.format(""),
                telNumber: jQuery.validator.format(""),
                password: jQuery.validator.format(""),
                confirmPassword: jQuery.validator.format(""),
            },
            submitHandler: function (form) {
                mySwiper.slideNext();
                $(".swiper-pagination-bullet-active")
                    .prev(".swiper-pagination-bullet")
                    .addClass("pointer");
                $(form).append('<input type="hidden" name="firstName" value="' + $("#firstName").val() + '"/>')
                    .append('<input type="hidden" name="lastName" value="' + $("#lastName").val() + '"/>')
                    .append('<input type="hidden" name="email" value="' + $("#email").val() + '"/>')
                    .append('<input type="hidden" name="number" value="' + $("#number").val() + '"/>')
                    .append('<input type="hidden" name="mobile" value="' + $("#mobile").val() + '"/>');

                $('.email-adress').text($("#email").val())
                $('.loader').css('display', 'flex')
                var formValues = $(form).serialize();

                $.post("/umbraco/api/registration/register", formValues, function (data) {
                    
                })
                .done(function (data) {
                    initResendEmailListener(data.Email);
                    $('.loader').hide()
                    $('.error-message').hide();
                    $('.sucess-message').show();
                })
                .fail(function () {
                    $('.loader').hide()
                    $('.error-message').show();
                    $('.sucess-message').hide()
                })
                //form.submit();
                return false;
            },
        });

    //custom mobile number validation
    $("#mobile").change(function (e) {
        var val = $(this).val();
        var reg = /^5/gi;
        if (val.match(reg) && val.length == 9) {
            $(this).css("border", "1px solid #3ac946");
            $("#mobile ~ .success-icon").css("display", "block");
            $("#mobile ~ .error-icon").hide();
        } else {
            $(this).css("border", "1px solid #f05544");
            $("#mobile ~.success-icon").hide();
            $("#mobile ~.error-icon").css("display", "block");
        }
    });

    if ($(".swiper-slide.last-slider")) {
        $(".swiper-pagination").hide();
    }
    $(".custom-select").each(function () {
        var classes = $(this).attr("class"),
            id = $(this).attr("id"),
            name = $(this).attr("name");
        var template = '<div class="' + classes + '">';
        template += '<span class="custom-select-trigger">' + $(this).attr("placeholder") + '</span>';
        template += '<div class="custom-options">';
        $(this).find("option").each(function () {
            template += '<span class="custom-option ' + $(this).attr("class") + '" data-value="' + $(this).attr("value") + '">' + $(this).html() + '</span>';
        });
        template += '</div></div>';

        $(this).wrap('<div class="custom-select-wrapper"></div>');
        $(this).hide();
        $(this).after(template);
    });
    //$(".custom-option:first-of-type").hover(function () {
    //    $(this).parents(".custom-options").addClass("option-hover");
    //}, function () {
    //    $(this).parents(".custom-options").removeClass("option-hover");
    //});
    $(".custom-select-trigger").on("click", function () {
        $('html').one('click', function () {
            $(".custom-select").removeClass("opened");
        });
        $(this).parents(".custom-select").toggleClass("opened");
        event.stopPropagation();
    });
    $(".custom-option").on("click", function () {
        $(this).parents(".custom-select-wrapper").find("select").val($(this).data("value"));
        $(this).parents(".custom-options").find(".custom-option").removeClass("selection");
        $(this).addClass("selection");
        $(".custom-select-trigger").css('border', ' 1px solid #3ac946');
        $(this).parents(".custom-select").removeClass("opened");
        $(this).parents(".custom-select").find(".custom-select-trigger").text($(this).text());
    });
    $('.submit').on('click',function () {
        if ($(".custom-option").hasClass('selection')) {
            $(".custom-select-trigger").css('border', ' 1px solid #3ac946');
        }
        else {
            $(".custom-select-trigger").css('border', ' 1px solid #f05544');
        }
        
    })

    function initResendEmailListener(requestEmail) {
        $("#resendEmail").on("click", (e) => {
            e.preventDefault();

            $(".loader").show();
            $('.error-message').hide();
            $('.sucess-message').hide();

            $.post("/umbraco/api/registration/resendEmail", { email: requestEmail})
            .done(function (data) {
                $('.loader').hide()
                $('.error-message').hide();
                $('.sucess-message').show();
            })
            .fail(function () {
                $('.loader').hide()
                $('.error-message').show();
                $('.sucess-message').hide()
            })
        })
    }
});