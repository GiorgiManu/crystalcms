﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CrystalCMS.Web.Helpers
{
    public class YutubeLinkConverter
    {
        public static string ConvertToEmbedUrl(string youtubeUrl)
        {
            if (!IsUrl(youtubeUrl))
                return string.Empty;

            var uri = new Uri(youtubeUrl);
            
            var query = HttpUtility.ParseQueryString(uri.Query);

            var videoId = string.Empty;

            if (query.AllKeys.Contains("v"))
                return IdToEmdedUrl(query["v"]);

            return IdToEmdedUrl(uri.Segments.Last());
        }

        public static string IdToEmdedUrl(string videoId)
        {
            return $"https://youtube.com/embed/{videoId}";
        }

        public static bool IsUrl(string url)
        {
            return Uri.IsWellFormedUriString(url, UriKind.Absolute);
        }
    }
}