﻿using CrystalCMS.Web.Extensions;
using CrystalCMS.Web.Models.Packages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core.Models.PublishedContent;

namespace CrystalCMS.Web.Helpers
{
    public static class PackagesHelper
    {
        public static IEnumerable<PackageFeatureViewModel> GetFeatures(IPublishedContent content)
        {
            return content.TryGetValue<IEnumerable<IPublishedElement>>("aboutSectionProgramsList")
                    ?.Select(x => new PackageFeatureViewModel());
        }

        public static IEnumerable<PackageFeatureType> GetFeatureTypes(IPublishedContent content)
        {
            return content.TryGetValue<IEnumerable<IPublishedElement>>("")?.Select(x => new PackageFeatureType
            {
                Type = x.TryGetValue<string>("featureTypeName")
            });
        }
    }
}