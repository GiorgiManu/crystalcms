﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CrystalCMS.Web.Models.Agreements
{
    public class AgreementsViewModel
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public IEnumerable<AgreementModel> AgreementSections { get; set; }
    }
    
    public class AgreementModel
    {
        public string Title { get; set; }
        public string Description { get; set; }
    }
}