﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CrystalCMS.Web.Models.About
{
    public class HistoryIntegrationsSectionViewModel : ViewModelBase
    {
        public ViewModelBase Integration { get; set; }
        public IEnumerable<string> Items { get; set; }
    }
}