﻿using CrystalCMS.Web.Models.Packages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CrystalCMS.Web.Models.Home
{
    public class PackagesSectionViewModel
    {
        public Package Standard { get; set; }
        public Package Premium { get; set; }

        public bool IsHomePage { get; set; }
        public string RegistrationUrl { get;set; }
    }
}