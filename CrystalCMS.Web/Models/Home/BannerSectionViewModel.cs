﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CrystalCMS.Web.Models.Home
{
    public class BannerSectionViewModel : ViewModelBase
    {
        public string VideoUrl { get; set; }
        public string RegistrationUrl { get; set; }
    }
}