﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CrystalCMS.Web.Models.Home
{
    public class PrioritySectionViewModel : ViewModelBase
    {
        public IEnumerable<PriorityItem> Items { get; set; }
    }

    public class PriorityItem : ViewModelBase
    {
    }
}