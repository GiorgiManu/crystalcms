﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CrystalCMS.Web.Models.Home
{
    public class FeatureSectionViewModel : ViewModelBase
    {
        public IEnumerable<FeatureItem> Items { get; set; }
    }

    public class FeatureItem : ViewModelBase
    {

    }
}