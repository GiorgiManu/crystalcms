﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CrystalCMS.Web.Models.Packages
{
    public class PackageFeatureViewModel
    {
        public string Description { get; set; }
        public string PackageType { get; set; }
    }

    public class PackageFeatureType
    {
        public string Type { get; set; }
    }
}