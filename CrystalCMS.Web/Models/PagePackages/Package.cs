﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CrystalCMS.Web.Models.Packages
{
    public class Package
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public int MonthlyPayment { get; set; }
        public int YearlyPayment { get; set; }
        public IEnumerable<PackageFeatureItem> Features { get; set; }
    }
    public class PackageFeatureItem
    {
        public bool IsOn { get; set; }
        public string Name { get; set; }
    }
}