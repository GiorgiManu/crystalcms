﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CrystalCMS.Web.Models.Packages
{
    public class AdvantagesSectionViewModel
    {
        public IEnumerable<PackageFeatureItem> Features { get; set; }
        public string RegistrationUrl { get; set; }

        public class PackageFeatureItem
        {
            public bool InPremium { get; set; }
            public bool InStandard { get; set; }
            public string Name { get; set; }
            public string Type { get; set; }
        }
    }
}