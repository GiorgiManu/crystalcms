﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CrystalCMS.Web.Models.Registration
{
    public class ErpResendEmailRequest
    {
        public string Email { get; set; }

        public static ErpResendEmailRequest FromCmsRequest(ErpResendEmailModel model)
        {
            return new ErpResendEmailRequest
            {
                Email = model.Email
            };
        }
    }

    public class ErpResendEmailResponse
    {
        public string Id { get; set; }
    }

    public class ErpResendEmailModel
    {
        public string Email { get; set; }
    }
}