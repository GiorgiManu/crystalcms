﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace CrystalCMS.Web.Models.Registration
{
    public class RegistrationModelRequest
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Number { get; set; }
        public string Email { get; set; }
        public string Adress { get; set; }
        public string Mobile { get; set; }
        public string TelNumber { get; set; }
        public string CompanyName { get; set; }
        public string IdentificationNumber { get; set; }
        public int CostCalculationType { get; set; }
    }

    public class RegistrationModelResponse
    {
        public int Status { get; set; }
        public object Data { get; set; }
        public IEnumerable<string> Errors { get; set; }
    }
}