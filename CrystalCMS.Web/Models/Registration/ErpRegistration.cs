﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CrystalCMS.Web.Models.Registration
{
    public class ErpRegistrationRequest
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Pin { get; set; }
        public string Mobile { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string IdentificationCode { get; set; }
        public string Address { get; set; }
        public string Name { get; set; }
        public int CostCalculationType { get; set; }

        public static ErpRegistrationRequest FromCmsRequest(RegistrationModelRequest request)
        {
            return new ErpRegistrationRequest
            {
                Address = request.Adress,
                Email = request.Email,
                FirstName = request.FirstName,
                IdentificationCode = request.IdentificationNumber,
                LastName = request.LastName,
                Mobile = request.Mobile,
                Phone = request.TelNumber,
                Pin = request.Number,
                Name = request.CompanyName,
                CostCalculationType = request.CostCalculationType
            };
        }
    }

    public class ErpRegistrationResponse
    {
        public Guid Id { get; set; }
        public string Email { get; set; }
    }
}