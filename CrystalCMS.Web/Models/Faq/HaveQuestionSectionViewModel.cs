﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CrystalCMS.Web.Models.Faq
{
    public class HaveQuestionSectionViewModel : ViewModelBase
    {
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string SubText { get; set; }
    }
}