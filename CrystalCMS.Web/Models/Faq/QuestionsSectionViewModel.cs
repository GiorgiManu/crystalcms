﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CrystalCMS.Web.Models.Faq
{
    public class QuestionsSectionViewModel
    {
        public IEnumerable<QuestionItem> Items { get; set; }
    }
    public class QuestionItem
    {
        public string Question { get; set; }
        public string Answer { get; set; }
    }
}