﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core.Models.PublishedContent;
using Umbraco.Web;

namespace CrystalCMS.Web.Extensions
{
    public static class UmbracoContentExtensions
    {
        public static T TryGetValue<T>(this IPublishedContent content, string propertyName)
            => content.HasValue(propertyName) ? content.Value<T>(propertyName) : default(T);

        public static T TryGetValue<T>(this IPublishedContent content, string propertyName, string culture)
            => content.HasValue(propertyName) ? content.Value<T>(propertyName, culture: culture) : default;

        public static T TryGetValue<T>(this IPublishedElement element, string propertyName)
            => element != null && element.HasValue(propertyName) ? element.Value<T>(propertyName) : default;

        public static object IfDefaultSet<T>(this object source, object newValue)
            => source is null || source == default ? newValue : source;
    }
}