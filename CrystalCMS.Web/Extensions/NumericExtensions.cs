﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CrystalCMS.Web.Extensions
{
    public static class NumericExtensions
    {
        public static bool IsEven(this int number)
            => number % 2 == 0;
    }
}