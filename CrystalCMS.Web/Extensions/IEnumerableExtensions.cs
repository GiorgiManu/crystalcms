﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CrystalCMS.Web.Extensions
{
    public static class IEnumerableExtensions
    {
        public static bool IsNullOrEmpty<T>(this IEnumerable<T> src)
            => (src is null && !src.Any());
        
        public static bool IsNotNullOrEmpty<T>(this IEnumerable<T> src)
            => (src != null && src.Any());
    }
}