﻿using CrystalCMS.Web.Extensions;
using CrystalCMS.Web.Helpers;
using CrystalCMS.Web.Models.Home;
using CrystalCMS.Web.Models.Packages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Umbraco.Core.Models.PublishedContent;
using Umbraco.Web;
using Umbraco.Web.Mvc;

namespace CrystalCMS.Web.Controllers
{
    public class HomePageHandlerController : SurfaceController
    {
        public ActionResult RenderBannerSection()
        {
            var content = Umbraco.Content(CurrentPage.Id);

            var model = new BannerSectionViewModel
            {
                Title = content.TryGetValue<string>("bannerTitle"),
                Description = content.TryGetValue<string>("bannerDescription"),
                VideoUrl = YutubeLinkConverter.ConvertToEmbedUrl(content.TryGetValue<string>("bannerVideoUrl")),
                ImageUrl = content.TryGetValue<IPublishedContent>("bannerImage")?.Url(),
                RegistrationUrl = Umbraco.Content(4211)?.Url()
            };

            return PartialView("Home/SectionBanner", model);
        }

        public ActionResult RenderPrioritySection()
        {
            var content = Umbraco.Content(CurrentPage.Id);

            var model = new PrioritySectionViewModel
            {
                Title = content.TryGetValue<string>("benefitsTitle"),
                Description = content.TryGetValue<string>("benefitsDescription"),
                Items = content.TryGetValue<IEnumerable<IPublishedElement>>("benefitsList")?.Select(x => new PriorityItem
                {
                    Title = x.TryGetValue<string>("benefitItemTitle"),
                    Description = x.TryGetValue<string>("benefitItemDescription"),
                    ImageUrl = x.TryGetValue<IPublishedContent>("benefitItemIcon")?.Url
                })
            };

            return PartialView("Home/SectionPriority", model);
        }

        public ActionResult RenderPosSection()
        {
            var content = Umbraco.Content(CurrentPage.Id);
            var model = new FeatureSectionViewModel
            {
                Title = content.TryGetValue<string>("posFeatureSectionTitle"),
                Items = content.TryGetValue<IEnumerable<IPublishedElement>>("posFeatureSectionList")?.Select(x => new FeatureItem
                {
                    Title = x.TryGetValue<string>("FeatureName"),
                    Description = x.TryGetValue<string>("FeatureDescription")
                })
            };

            return PartialView("Home/SectionPos", model);
        }

        public ActionResult RenderErpSection()
        {
            var content = Umbraco.Content(CurrentPage.Id);
            var model = new FeatureSectionViewModel
            {
                Title = content.TryGetValue<string>("erpFeatureSectionTitle"),
                Items = content.TryGetValue<IEnumerable<IPublishedElement>>("erpFeatureSectionList")?.Select(x => new FeatureItem
                {
                    Title = x.TryGetValue<string>("FeatureName"),
                    Description = x.TryGetValue<string>("FeatureDescription")
                })
            };

            return PartialView("Home/SectionErp", model);
        }

        public ActionResult RenderPackagesSection()
        {
            var content = Umbraco.Content(Guid.Parse("952fc9d0-fee7-4b2b-8afe-fdd656ae5a88"));
            var features = Umbraco.Content(Guid.Parse("7c5dd8d1-c848-4bbd-9fe3-9c1234f828cf"))
                .ChildrenOfType("packageFeature");

            var model = PackagesPageHandlerController.GetPackages(content, features);
            model.RegistrationUrl = Umbraco.Content(4211)?.Url();
            model.IsHomePage = true;

            return PartialView("PagePackages/SectionPackages", model);
        }
        public ActionResult RenderRequestDemoSection()
        {
            var content = Umbraco.Content(CurrentPage.Id);

            var model = new RequestDemoViewModel
            {
                Title = content.TryGetValue<string>("requestDemoSectionTitle"),
                Description = content.TryGetValue<string>("requestDemoSectionDescription"),
                ButtonText = content.TryGetValue<string>("requestDemoSectionButtonText"),
                ButtonUrl = content.TryGetValue<string>("requestDemoSectionButtonUrl")
            };

            return PartialView("Home/SectionRequestDemo", model);
        }
    }
}