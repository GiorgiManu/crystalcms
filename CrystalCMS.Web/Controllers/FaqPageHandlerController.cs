﻿using CrystalCMS.Web.Extensions;
using CrystalCMS.Web.Models;
using CrystalCMS.Web.Models.Faq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Umbraco.Core.Models.PublishedContent;
using Umbraco.Web.Mvc;

namespace CrystalCMS.Web.Controllers
{
    public class FaqPageHandlerController : SurfaceController
    {
        public ActionResult RenderMainSection()
        {
            var content = Umbraco.Content(CurrentPage.Id);
            var model = new ViewModelBase
            {
                Title = content.TryGetValue<string>("mainSectionTitle"),
                Description = content.TryGetValue<string>("mainSectionDescription")
            };

            return PartialView("SectionMain", model);
        }

        public ActionResult RenderQuestionsSection()
        {
            var content = Umbraco.Content(CurrentPage.Id);
            var model = new QuestionsSectionViewModel
            {
                Items = content.TryGetValue<IEnumerable<IPublishedElement>>("faqQuestionsList")?.Select(x => new QuestionItem
                {
                    Question = x.TryGetValue<string>("faqQuestion"),
                    Answer = x.TryGetValue<string>("faqAnswer")
                })
            };

            return PartialView("Faq/SectionQuestions", model);
        }

        public ActionResult RenderContactSection()
        {
            var content = Umbraco.Content(CurrentPage.Id);
            var model = new HaveQuestionSectionViewModel
            {
                Title = content.TryGetValue<string>("faqHaveQuestionTitle"),
                Description = content.TryGetValue<string>("faqHaveQuestionDescription"),
                Email = content.TryGetValue<string>("contactEmail"),
                PhoneNumber = content.TryGetValue<string>("contactPhoneNumber"),
                SubText = content.TryGetValue<string>("contactDescription")
            };

            return PartialView("Faq/SectionContact", model);
        }
    }
}