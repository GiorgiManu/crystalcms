﻿using CrystalCMS.Web.Extensions;
using CrystalCMS.Web.Models;
using CrystalCMS.Web.Models.About;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Umbraco.Core.Models.PublishedContent;
using Umbraco.Web.Mvc;

namespace CrystalCMS.Web.Controllers
{
    public class AboutPageHandlerController : SurfaceController
    {
        public ActionResult RenderMainSection()
        {
            var content = Umbraco.Content(CurrentPage.Id);
            var model = new ViewModelBase
            {
                Title = content.TryGetValue<string>("mainSectionTitle"),
                Description = content.TryGetValue<string>("mainSectionDescription")
            };

            return PartialView("SectionMain", model);
        }

        public ActionResult RenderHistoryIntegrationsSection()
        {
            var content = Umbraco.Content(CurrentPage.Id);
            var model = new HistoryIntegrationsSectionViewModel
            {
                Title = content.TryGetValue<string>("aboutHistoryTitle"),
                Description = content.TryGetValue<string>("aboutHistoryDescription"),
                Items = content.TryGetValue<IEnumerable<string>>("aboutIntegrations"),
                ImageUrl = content.TryGetValue<IPublishedContent>("aboutImage")?.Url,
                Integration = new ViewModelBase
                {
                    Title = content.TryGetValue<string>("integrationTitle"),
                    Description = content.TryGetValue<string>("integrationDescription")
                }
            };

            return PartialView("About/SectionHistoryIntegrations", model);
        }
    }
}