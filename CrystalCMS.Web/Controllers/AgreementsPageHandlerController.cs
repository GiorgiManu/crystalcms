﻿using CrystalCMS.Web.Extensions;
using CrystalCMS.Web.Models;
using CrystalCMS.Web.Models.Agreements;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Umbraco.Core.Models.PublishedContent;
using Umbraco.Web.Mvc;

namespace CrystalCMS.Web.Controllers
{
    public class AgreementsPageHandlerController : SurfaceController
    {
        public ActionResult RenderMainSection()
        {
            var content = Umbraco.Content(CurrentPage.Id);
            var model = new ViewModelBase
            {
                Title = content.TryGetValue<string>("mainSectionTitle"),
                Description = content.TryGetValue<string>("mainSectionDescription")
            };

            return PartialView("SectionMain", model);
        }

        public ActionResult RenderAgreementsSection()
        {
            var content = Umbraco.Content(CurrentPage.Id);
            var model = new AgreementsViewModel
            {
                Title = content.TryGetValue<string>("agreementsTitle"),
                Description = content.TryGetValue<string>("agreementsDescription"),
                AgreementSections = content.TryGetValue<IEnumerable<IPublishedElement>>("sections")?
                .Select(x => new AgreementModel
                {
                    Title = x.TryGetValue<string>("agreementTitle"),
                    Description = x.TryGetValue<string>("agreementDescription")
                })
            };

            return PartialView("Agreements/SectionAgreements", model);
        }
    }
}