﻿using CrystalCMS.Web.Models.Registration;
using Newtonsoft.Json;
using System.Configuration;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using Umbraco.Web.WebApi;

namespace CrystalCMS.Web.Controllers
{
    public class RegistrationController : UmbracoApiController
    {
        private readonly string erpApiUrl;

        public RegistrationController()
        {
            erpApiUrl = ConfigurationManager.AppSettings["ErpApiUrl"];
        }

        [System.Web.Mvc.HttpPost]
        public async Task<IHttpActionResult> ResendEmail([FromBody] ErpResendEmailModel model)
        {
            if (model is null) return BadRequest();

            HttpResponseMessage erpResponse = default;

            var erpRequestModel = ErpResendEmailRequest.FromCmsRequest(model);
            var json = JsonConvert.SerializeObject(erpRequestModel);

            using (var client = new HttpClient())
            {
                erpResponse = await client.PostAsync($"{erpApiUrl}/api/v1/administration/resend-registration-email", new StringContent(json, Encoding.UTF8, "application/json"));
            }

            if (erpResponse.IsSuccessStatusCode)
            {
                var data = JsonConvert.DeserializeObject<ErpResendEmailResponse>(await erpResponse.Content.ReadAsStringAsync());
                return Ok(data);
            }

            return BadRequest();
        }

        [System.Web.Mvc.HttpPost]
        public async Task<IHttpActionResult> Register([FromBody] RegistrationModelRequest model)
        {
            if (model is null) return BadRequest();
            
            HttpResponseMessage erpResponse = default;

            var erpRequestModel = ErpRegistrationRequest.FromCmsRequest(model);
            var json = JsonConvert.SerializeObject(erpRequestModel);
            
            using (var client = new HttpClient())
            {
                erpResponse = await client.PostAsync($"{erpApiUrl}/api/v1/administration", new StringContent(json, Encoding.UTF8, "application/json"));
            }

            if (erpResponse.IsSuccessStatusCode)
            {
                var data = JsonConvert.DeserializeObject<ErpRegistrationResponse>(await erpResponse.Content.ReadAsStringAsync());
                data.Email = erpRequestModel.Email;

                return Ok(data);
            }

            return BadRequest();
        }
    }
}