﻿using CrystalCMS.Web.Extensions;
using CrystalCMS.Web.Helpers;
using CrystalCMS.Web.Models;
using CrystalCMS.Web.Models.Home;
using CrystalCMS.Web.Models.Packages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Umbraco.Core.Models.PublishedContent;
using Umbraco.Web;
using Umbraco.Web.Mvc;

namespace CrystalCMS.Web.Controllers
{
    public class PackagesPageHandlerController : SurfaceController
    {
        public ActionResult RenderMainSection()
        {
            var content = Umbraco.Content(CurrentPage.Id);
            var model = new ViewModelBase
            {
                Title = content.TryGetValue<string>("mainSectionTitle"),
                Description = content.TryGetValue<string>("mainSectionDescription")
            };

            return PartialView("SectionMain", model);
        }

        public ActionResult RenderPackagesSection()
        {
            var content = Umbraco.Content(CurrentPage.Id);
            var features = Umbraco.Content(Guid.Parse("7c5dd8d1-c848-4bbd-9fe3-9c1234f828cf"))
                .ChildrenOfType("packageFeature");

            var model = GetPackages(content, features);
            model.RegistrationUrl = Umbraco.Content(4211)?.Url();
            
            return PartialView("PagePackages/SectionPackages", model);
        }

        public ActionResult RenderAdvantagesSection()
        {
            var content = Umbraco.Content(CurrentPage.Id);
            var features = Umbraco.Content(Guid.Parse("7c5dd8d1-c848-4bbd-9fe3-9c1234f828cf"))
                .ChildrenOfType("packageFeature");

            var model = new AdvantagesSectionViewModel
            {
                Features = features?.Select(x => new AdvantagesSectionViewModel.PackageFeatureItem
                {
                    InStandard = x.TryGetValue<bool>("inStandard"),
                    InPremium = x.TryGetValue<bool>("inPremium"),
                    Name = x.Name,
                    Type = x.TryGetValue<IPublishedContent>("featureType")?.TryGetValue<string>("featureTypeName")
                }),
                RegistrationUrl = Umbraco.Content(4211)?.Url()
            };

            return PartialView("PagePackages/Advantages", model);
        }


        public static PackagesSectionViewModel GetPackages(IPublishedContent content, IEnumerable<IPublishedContent> features)
        {
            var model = new PackagesSectionViewModel
            {
                Standard = new Package
                {
                    Name = content.TryGetValue<string>("standardName"),
                    Description = content.TryGetValue<string>("standardDescription"),
                    MonthlyPayment = content.TryGetValue<int>("standardMonthlyPayment"),
                    YearlyPayment = content.TryGetValue<int>("standardYearlyPayment"),
                    Features = features.Where(x => x.TryGetValue<bool>("showInStandardPackage"))?.Select(x => new PackageFeatureItem
                    {
                        IsOn = x.TryGetValue<bool>("inStandard"),
                        Name = x.Name
                    }).OrderByDescending(x => x.IsOn)
                },
                Premium = new Package
                {
                    Name = content.TryGetValue<string>("premiumName"),
                    Description = content.TryGetValue<string>("premiumDescription"),
                    MonthlyPayment = content.TryGetValue<int>("premiumMonthlyPayment"),
                    YearlyPayment = content.TryGetValue<int>("premiumYearlyPayment"),
                    Features = features.Where(x => x.TryGetValue<bool>("showInPremiumPackage"))?.Select(x => new PackageFeatureItem
                    {
                        IsOn = x.TryGetValue<bool>("inPremium"),
                        Name = x.Name
                    }).OrderByDescending(x => x.IsOn)
                }
            };

            return model;
        }
    }
}